//
//  TrailerDataLoader.swift
//  SwiftUISuperheroAPI
//
//  Created by Catherine Rohovska on 05.01.2022.
//

import Foundation
import SwiftUI
import Combine
import Foundation

final class TrailerDataLoader: ObservableObject {
    @Published var trailer = FilmTrailerModel()
    
    @Published var state = LoadingState.ready
    var dataTask: AnyPublisher<FilmTrailerModel, Error>!
    private var session = URLSession.shared;
    
    func load(filmId: String) {
        if let url = URL(string: ApiEndpoints.environment.rawValue + ApiEndpoints.trailer.rawValue + ApiKeys.imdb.rawValue + "/" + filmId) {
            dataTask = self.session
                .dataTaskPublisher(for: url)
                .map { $0.data }
                .decode(type: FilmTrailerModel.self, decoder: JSONDecoder())
                .receive(on: RunLoop.main)
                .eraseToAnyPublisher()
            
            self.state = .loading(self.dataTask.sink(
                        receiveCompletion: { completion in
                            switch completion {
                            case .finished:
                                break
                            case let .failure(error):
                                self.state = .error(error)
                            }
                        },
                        receiveValue: { value in
                            self.state = .loaded
                            self.trailer = value
                        }
                    ))
        }
            
    }


}

