//
//  FilmTrailerModel.swift
//  SwiftUISuperheroAPI
//
//  Created by Catherine Rohovska on 05.01.2022.
//

import Foundation

struct FilmTrailerModel: Codable {
    var imDbId: String?
    var videoTitle: String?
    var link: String?
    var videoDescription: String?
    var fullTitle: String?
    var linkEmbed: String?
}

extension FilmTrailerModel: Identifiable {
    var id: String? { return imDbId}
    var title: String? { return videoTitle}
    var trailerUrl: String? { return linkEmbed}
    var description: String? {return videoDescription?.convertSpecialCharacters()}
}
