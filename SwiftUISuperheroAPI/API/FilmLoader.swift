//
//  Superhero.swift
//  SwiftUISuperheroAPI
//
//  Created by Catherine Rohovska on 05.01.2022.
//

import Foundation
import SwiftUI
import Combine

final class FilmLoader: ObservableObject {
    
    private var session = URLSession.shared;
    
    @Published var films = [FilmModel]()
    @Published var state = LoadingState.ready
    
    var dataTask: AnyPublisher<SearchFilmResponseModel, Error>!

    
    func fetchSearchData(search: String = "") {
        let config = URLSessionConfiguration.default
        session = URLSession(configuration: config)
        
        guard let search = search.addingPercentEncoding(withAllowedCharacters: .alphanumerics) else {
            let error = NSError(domain: CustomErrorDomains.wrongSearchParameters.rawValue, code: 0, userInfo: nil)
            self.state = .error(error)
            return;
        }
        
        let components = URLComponents(string: ApiEndpoints.environment.rawValue + ApiEndpoints.search.rawValue + ApiKeys.imdb.rawValue + "/" + search)
        
        guard let url = components?.url else {
            let error = NSError(domain: CustomErrorDomains.wrongUrl.rawValue, code: 0, userInfo: nil)
            self.state = .error(error)
            return;
        }
        
        dataTask = self.session
            .dataTaskPublisher(for: url)
            .map { $0.data }
            .decode(type: SearchFilmResponseModel.self, decoder: JSONDecoder())
            .receive(on: RunLoop.main)
            .eraseToAnyPublisher()
        
        self.state = .loading(self.dataTask.sink(
                    receiveCompletion: { completion in
                        switch completion {
                        case .finished:
                            break
                        case let .failure(error):
                            self.state = .error(error)
                        }
                    },
                    receiveValue: { value in
                        self.state = .loaded
                        self.films = value.results
                    }
                ))
    }

}
