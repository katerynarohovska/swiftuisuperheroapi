//
//  LoadingIndicator.swift
//  SwiftUISuperheroAPI
//
//  Created by Catherine Rohovska on 05.01.2022.
//

import Foundation
import SwiftUI

struct LoadingIndicator: View {

    @Binding var isShowing: Bool

    var body: some View {
                VStack {
                    Text("Loading")
                    ActivityIndicator(isAnimating: .constant(true), style: .large)
                }
                .frame(width: 200, height: 200, alignment: .center)
                .background(Color.secondary.colorInvert())
                .foregroundColor(Color.primary)
                .cornerRadius(20)
                .opacity(self.isShowing ? 1 : 0)

    }

}
