//
//  String+Extension.swift
//  SwiftUISuperheroAPI
//
//  Created by Catherine Rohovska on 10.01.2022.
//

import Foundation
extension String {
    func convertSpecialCharacters() -> String {
            var newString = self
            let char_dictionary = [
                "&amp;" : "&",
                "&lt;" : "<",
                "&gt;" : ">",
                "&quot;" : "\"",
                "&apos;" : "'"
            ];
            for (escaped_char, unescaped_char) in char_dictionary {
                newString = newString.replacingOccurrences(of: escaped_char, with: unescaped_char, options: NSString.CompareOptions.literal, range: nil)
            }
            return newString
    }
}
