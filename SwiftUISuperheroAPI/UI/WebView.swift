//
//  WebView.swift
//  SwiftUISuperheroAPI
//
//  Created by Catherine Rohovska on 10.01.2022.
//

import Foundation
import SwiftUI
import WebKit

struct WebView : UIViewRepresentable {
    
    let request: URLRequest
    
    func makeUIView(context: Context) -> WKWebView  {
        let view = WKWebView()
        view.contentMode = .center
        return view
    }
    
    func updateUIView(_ uiView: WKWebView, context: Context) {
        uiView.load(request)
    }
    
}
