//
//  Endpoints.swift
//  SwiftUISuperheroAPI
//
//  Created by Catherine Rohovska on 05.01.2022.
//

import Foundation
import Combine

enum ApiKeys: String {
    case imdb = "k_38o9u733"
}

enum ApiEndpoints: String {
    case environment = "https://imdb-api.com/en/API"
    case search = "/SearchTitle/"
    case trailer = "/Trailer/"
}

enum CustomErrorDomains: String {
    case dataDecoding = "CustomDataDecoding"
    case wrongUrl = "CustomWrongURL"
    case wrongSearchParameters = "CustomWrongSearch"
}

enum LoadingState {
    case ready
    case loading(Cancellable)
    case loaded
    case error(Error)
}
