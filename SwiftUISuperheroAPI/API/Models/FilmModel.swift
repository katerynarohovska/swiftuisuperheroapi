//
//  HeroModel.swift
//  SwiftUISuperheroAPI
//
//  Created by Catherine Rohovska on 05.01.2022.
//

import Foundation
struct FilmModel: Identifiable, Codable {
    var id: String
    var title: String
    var image: String = ""
    var description: String = ""
}

