//
//  FilmTrailerDetailView.swift
//  SwiftUISuperheroAPI
//
//  Created by Catherine Rohovska on 05.01.2022.
//

import Foundation
import SwiftUI

struct FilmTrailerDetailView: View {
    @Environment(\.presentationMode) var presentationMode
    @ObservedObject var model: TrailerDataLoader
    var film: FilmModel
    
    init(film: FilmModel) {
        self.film = film
        self.model = TrailerDataLoader()
    }
    
    var body: some View {
        ScrollView {
            VStack {
                switch model.state {
                case .ready, .loading(_):
                    AnyView(LoadingIndicator(isShowing: .constant(true)))
                        .padding(.all, 5.0)
                case let .error(error):
                    AnyView(Text(error.localizedDescription))
                case .loaded:
                    Text(model.trailer.description ?? "No description available")
                    if let link = URL(string: model.trailer.trailerUrl ?? "") {
                        WebView(request: URLRequest(url: link))
                            .frame(minWidth: 0, maxWidth: .infinity)
                            .frame(height: 506)
                            .padding(.horizontal, 15)
                    } else {
                        VStack{
                            Text("Sorry, no video trailer available")
                                .fontWeight(.medium)
                                .foregroundColor(Color.red)
                                .multilineTextAlignment(.center)
                            ImageLoaderView(url: URL(string: film.image)!) {
                                LoadingIndicator(isShowing: .constant(true))
                            }.padding(.all, 15)
                        }
                        
                    }
                }
            }
    }.navigationBarTitle(film.title)
            .onAppear {
                model.load(filmId: film.id)
            }
    }
}
