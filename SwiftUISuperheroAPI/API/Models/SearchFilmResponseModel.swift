//
//  SearchFilmResponseModel.swift
//  SwiftUISuperheroAPI
//
//  Created by Catherine Rohovska on 05.01.2022.
//

import Foundation
struct SearchFilmResponseModel: Codable {
    var results: [FilmModel]
}
