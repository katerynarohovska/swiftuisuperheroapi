//
//  EmptyDataViewModifier.swift
//  SwiftUISuperheroAPI
//
//  Created by Catherine Rohovska on 05.01.2022.
//

import Foundation
import SwiftUI

struct EmptyDataModifier<Placeholder: View>: ViewModifier {

    let items: [Any]
    let placeholder: Placeholder

    @ViewBuilder
    func body(content: Content) -> some View {
        if !items.isEmpty {
            content
        } else {
            placeholder
        }
    }
}
