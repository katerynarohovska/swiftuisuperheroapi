//
//  ContentView.swift
//  SwiftUISuperheroAPI
//
//  Created by Catherine Rohovska on 05.01.2022.
//

import SwiftUI

struct ContentView: View {
    
    @State private var searchText = ""
    @State private var selectedFilm: FilmModel?
    @ObservedObject var model = FilmLoader()
    
    var body: some View {
        NavigationView {
            VStack {
                switch model.state {
                case .ready:
                    AnyView(Text("Empty list")
                                .font(.largeTitle)
                                .fontWeight(.medium)
                                .foregroundColor(Color.gray)
                                .multilineTextAlignment(.center))
                case .loading(_):
                    AnyView(LoadingIndicator(isShowing: .constant(true)))
                        .padding(.all, 5.0)
                case let .error(error):
                    AnyView(Text(error.localizedDescription))
                case .loaded:
                    List(self.model.films) { film in
                        NavigationLink {
                            FilmTrailerDetailView(film: film)
                        } label: {
                            FilmTitleCell(film: film)
                        }
                    }.emptyListPlaceholder(self.model.films, AnyView(Text("Empty list")
                                                                        .font(.largeTitle)
                                                                        .fontWeight(.medium)
                                                                        .foregroundColor(Color.gray)
                                                                        .multilineTextAlignment(.center)))
                    
                }
            }.navigationTitle("Search films")
        }
        .navigationViewStyle(.stack)
        .searchable(text: $searchText, prompt: "Title, year, etc.")
        .onSubmit(of: .search) {
            print("search submitted")
            model.fetchSearchData(search: searchText)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
