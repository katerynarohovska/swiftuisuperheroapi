//
//  FilmTitleCell.swift
//  SwiftUISuperheroAPI
//
//  Created by Catherine Rohovska on 05.01.2022.
//

import Foundation
import SwiftUI

struct FilmTitleCell: View {
    var film: FilmModel
    
    var body: some View {
        HStack {
            ImageLoaderView(url: URL(string: film.image)!) {
                Text("Loading")
            }
            .frame(width: 60, height: 90, alignment: Alignment.center)
            VStack(alignment: .leading) {
                Text(film.title)
                    .font(.headline)
                    .padding(.horizontal, 5.0)
                Text(film.description)
                    .font(.subheadline)
                    .padding(.horizontal, 5.0)
            }
        }
        .frame(maxWidth: .infinity, alignment: .leading)
        .contentShape(Rectangle())
    }
}
