//
//  SwiftUISuperheroAPIApp.swift
//  SwiftUISuperheroAPI
//
//  Created by Catherine Rohovska on 05.01.2022.
//

import SwiftUI

@main
struct SwiftUISuperheroAPIApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
